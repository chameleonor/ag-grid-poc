import React, { Component } from "react";
import PropTypes from "prop-types";

import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";

const deepClone = value => JSON.parse(JSON.stringify(value));

class Grid extends Component {
  constructor(props) {
    super(props);

    this.setInitialRowData = this.setInitialRowData.bind(this);
    this.filterFilledRows = this.filterFilledRows.bind(this);
    this.addNewRow = this.addNewRow.bind(this);
    this.getAllRows = this.getAllRows.bind(this);

    this.grid = null;
  }

  static propTypes = {
    props: PropTypes.object,
    columnDefs: PropTypes.array,
    defaultData: PropTypes.array,
    rowData: PropTypes.array,
    handleGridChange: PropTypes.func
  };

  componentDidUpdate(prevProps, nextState) {
    const { rowData } = this.props;

    if (rowData.length) {
      const { api, columnApi } = this.grid;
      const lastRowData = rowData.splice(-1)[0];
      // const { gridApi, columnApi } = lastRowData;
      const lastGridRowIndex = api.getLastDisplayedRow();

      if (
        lastRowData.rowIndex === lastGridRowIndex &&
        this.validateRowData(lastRowData.data, columnApi)
      ) {
        console.log(
          " validation ",
          lastRowData.rowIndex === lastGridRowIndex,
          this.validateRowData(lastRowData.data, columnApi)
        );
        this.addNewRow(api, this.props.defaultData);
      }
    }

    this.grid.api.sizeColumnsToFit();
  }

  addNewRow(api, newData) {
    const { defaultData } = this.props;
    api.batchUpdateRowData({
      add: deepClone(defaultData)
    });
  }

  onGridReady(grid) {
    this.grid = grid;
    this.setInitialRowData(grid);
    grid.api.sizeColumnsToFit();
  }

  setInitialRowData(grid) {
    const { api } = grid;
    const { defaultData, rowData } = this.props;
    const initialRowData = rowData.length ? rowData : deepClone(defaultData);

    api.setRowData(initialRowData);
  }

  filterFilledRows(rows, grid) {
    return rows.filter(
      row => this.validateRowData(row.data, grid.columnApi) && row
    );
  }

  validateRowData(rowData, columnApi) {
    const columns = columnApi.getAllDisplayedVirtualColumns();
    return columns.every(column => rowData[column.colId]);
  }

  onRowEditingStopped(grid) {
    const { handleGridChange } = this.props;
    const rows = this.getAllRows(grid);
    const filteredRows = this.filterFilledRows(rows, grid);

    // const newData = [...filteredRows];
    // grid.api.batchUpdateRowData({
    //   update: newData.map(row => row.data)
    // });

    if (filteredRows.length) handleGridChange(filteredRows);
  }

  getAllRows(grid) {
    const { api } = grid;
    let rowData = [];
    api.forEachNode(node => rowData.push(node));
    return rowData;
  }

  render() {
    const { columnDefs } = this.props;
    return (
      <div
        className="ag-theme-balham"
        style={{
          height: "300px",
          width: "600px"
        }}
      >
        <AgGridReact
          columnDefs={columnDefs}
          // rowData={rowData}
          onGridReady={this.onGridReady.bind(this)}
          onRowEditingStopped={this.onRowEditingStopped.bind(this)}
          stopEditingWhenGridLosesFocus={true}
          editType={"fullRow"},
          onStartEditingCell={this.onStartEditingCell.bind(this)}
        ></AgGridReact>
      </div>
    );
  }
}

export default Grid;
