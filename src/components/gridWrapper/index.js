import React, { Component } from "react";
import ReactJson from "react-json-view";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";
import {
  Container,
  AppBar,
  Toolbar,
  Grid,
  Button,
  Paper,
  ButtonGroup,
} from "@material-ui/core";
import styles from "./";

import GridComponent from "components/grid";

import APP_CONSTS from "appConsts";
const {
  SUBROLE_GRID_COLUMN_DEFS,
  SUBROLE_GRID_DEFAULT_DATA,
  WEBHOOK_GRID_DEFAULT_DATA,
  WEBHOOK_GRID_COLUMN_DEFS,
} = APP_CONSTS;

class GridWrapper extends Component {
  constructor(props) {
    super(props);

    this.renderSubRoleGrid = this.renderSubRoleGrid.bind(this);
    this.renderWebhookGrid = this.renderWebhookGrid.bind(this);
    this.handleGridChange = this.handleGridChange.bind(this);

    this.state = {
      subRoleGrid: {
        rowData: [],
        jsonData: [],
      },
      webhookGrid: {
        rowData: [],
        jsonData: [],
      },
      showGrid: 1,
    };
  }

  handleGridChange(rowData, gridType) {
    let jsonData;

    if (gridType === "subRoleGrid")
      jsonData = rowData.map((row) => row.data.subRole);
    if (gridType === "webhookGrid") jsonData = rowData.map((row) => row.data);

    this.setState({
      [gridType]: {
        rowData,
        jsonData,
      },
    });
  }

  renderSubRoleGrid() {
    const { subRoleGrid } = this.state;
    const { rowData, jsonData } = subRoleGrid;
    const gridType = "subRoleGrid";

    return (
      <>
        <h1>SuRoles {jsonData.length}</h1>
        <GridComponent
          columnDefs={SUBROLE_GRID_COLUMN_DEFS}
          defaultData={SUBROLE_GRID_DEFAULT_DATA}
          rowData={rowData}
          handleGridChange={(rowData) =>
            this.handleGridChange(rowData, gridType)
          }
        />
        <ReactJson
          defaultValue={SUBROLE_GRID_DEFAULT_DATA}
          src={jsonData}
          theme="monokai"
        />
      </>
    );
  }

  renderWebhookGrid() {
    const { webhookGrid } = this.state;
    const { rowData, jsonData } = webhookGrid;
    const gridType = "webhookGrid";

    return (
      <>
        <h1>Webhook {jsonData.length}</h1>
        <GridComponent
          columnDefs={WEBHOOK_GRID_COLUMN_DEFS}
          defaultData={WEBHOOK_GRID_DEFAULT_DATA}
          rowData={rowData}
          handleGridChange={(rowData) =>
            this.handleGridChange(rowData, gridType)
          }
        />
        <ReactJson
          defaultValue={WEBHOOK_GRID_DEFAULT_DATA}
          src={jsonData}
          theme="monokai"
        />
      </>
    );
  }

  render() {
    // const { classes } = this.props;
    return (
      <div>
        <Container>
          <AppBar position="static">
            <Toolbar>
              {/* <ButtonGroup color="secondary" aria-label="select grid">
                <Button onClick={() => this.setState({ showGrid: 1 })}>
                  Sub Roles
                </Button>
                <Button onClick={() => this.setState({ showGrid: 0 })}>
                  Webhook
                </Button>
              </ButtonGroup> */}
            </Toolbar>
          </AppBar>
        </Container>

        <Container>
          <Paper variant="outlined" square>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              {/* {this.state.showGrid
                ? this.renderSubRoleGrid()
                : this.renderWebhookGrid()} */}
              {/* {this.renderSubRoleGrid()} */}
              {this.renderWebhookGrid()}
            </Grid>
          </Paper>
        </Container>
      </div>
    );
  }
}

GridWrapper.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(GridWrapper);
