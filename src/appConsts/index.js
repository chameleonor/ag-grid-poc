const SUBROLE_GRID_COLUMN_DEFS = [
  {
    headerName: "Sub Role",
    field: "subRole",
    editable: true,
  },
];

const WEBHOOK_GRID_COLUMN_DEFS = [
  { headerName: "1", field: "1", editable: true, sortable: true },
  { headerName: "2", field: "2", editable: true, sortable: true },
  { headerName: "3", field: "3", editable: true, sortable: true },
  { headerName: "4", field: "4", editable: true, sortable: true },
  { headerName: "5", field: "5", editable: true, sortable: true },
  { headerName: "6", field: "6", editable: true, sortable: true },
  { headerName: "7", field: "7", editable: true, sortable: true },
  { headerName: "8", field: "8", editable: true, sortable: true },
  { headerName: "9", field: "9", editable: true, sortable: true },
  { headerName: "10", field: "10", editable: true, sortable: true },
];

const SUBROLE_GRID_DEFAULT_DATA = [{ subRole: "" }];

const WEBHOOK_GRID_DEFAULT_DATA = [{ key: "", value: "" }];

export default {
  SUBROLE_GRID_COLUMN_DEFS,
  SUBROLE_GRID_DEFAULT_DATA,
  WEBHOOK_GRID_DEFAULT_DATA,
  WEBHOOK_GRID_COLUMN_DEFS,
};
