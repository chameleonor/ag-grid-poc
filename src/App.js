import React from "react";
import GridWrapper from "./components/gridWrapper";

const App = () => {
  return (
    <div className="App">
      <GridWrapper></GridWrapper>
    </div>
  );
};

export default App;
